// This file is run through browserify to produce bundle.js
// See the `update-web-version` script in package.json
// TODO: find a way to drop browserify; seems like overkill

const utter = require("../utterance-expander");

document.addEventListener("DOMContentLoaded",function() {
    document.querySelectorAll("button").forEach(elem => {
        elem.onclick=expandomatic;
    });
},false);

function expandomatic(event) {
    // get the input text as an array, as that's what utter.expand() expects
    const source = document.getElementById("source").value.split("\n");
    const output = document.getElementById("output");
    if (event.target.id === "expandtext") {
        output.innerHTML = utter.expand(source).join("\n");
    } else {
        output.innerHTML = JSON.stringify(utter.expand(source), null, 4);
    }
    window.getSelection().selectAllChildren(output);
    try {
        document.execCommand("copy");
    } catch (err) {
        console.error("Unable to automatically copy output to the clipboard");
    }
}
