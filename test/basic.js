const tap = require("tap");
const utter = require("../utterance-expander.js");

tap.same(
    utter.expand([
        "single line of text with no macros"
    ]),
    ["single line of text with no macros"],
    "single line of text with no macros"
);

tap.same(
    utter.expand([]),
    [],
    "empty input"
);

tap.same(
    utter.expand([
        "{SlotName} strings should pass unchanged",
        "Regardless of the {Position} where they {Occur} in the input"
    ]),
    [
        "{SlotName} strings should pass unchanged",
        "Regardless of the {Position} where they {Occur} in the input"
    ],
    "{SlotName} delimiters"
);

tap.same(
    utter.expand([
        "this should (produce/emit) two lines of output"
    ]),
    [
        "this should produce two lines of output",
        "this should emit two lines of output"
    ],
    "simplest a/b macro"
);

tap.same(
    utter.expand([
        "how about this \"input\""
    ],[
        {replace: "\"", with: ""}
    ]),
    [
        "how about this input"
    ],
    "multiple replacements of single char"
);

tap.same(
    utter.expand([
        "input + with? lots! of. regex chars that need |escaping"
    ],[
        {replace: "+", with: ""},
        {replace: "?", with: "foo"},
        {replace: ".", with: ","},
        {replace: "|", with: "pipe"}
    ]),
    [
        "input withfoo lots! of, regex chars that need pipeescaping"
    ],
    "multiple replacement chars which need escaping"
)
