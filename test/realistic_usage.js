const tap = require("tap");
const utter = require("../utterance-expander.js");

tap.same(
    utter.expand([
        "(hello/nihao) world",
        "this input contains no macros",
        "here (is an input/are some inputs) with (two/multiple) macros"
    ]),
    [
        "hello world",
        "nihao world",
        "this input contains no macros",
        "here is an input with two macros",
        "here are some inputs with two macros",
        "here is an input with multiple macros",
        "here are some inputs with multiple macros"
    ],
    "realistic valid input sample"
);

tap.same(
    utter.expand([
        "(this/that) will be the same",
        "this will be (the same/similar)"
    ]),
    [
        "this will be the same",
        "that will be the same",
        "this will be similar"
    ],
    "filter duplicates from output"
);

tap.same(
    utter.expand([
        " this line has  (unnecessary/extra) whitespace ",
        "this does (too/)"
    ]),
    [
        "this line has unnecessary whitespace",
        "this line has extra whitespace",
        "this does too",
        "this does"
    ],
    "remove extraneous whitespace"
);

tap.same(
    utter.expand([
        "this (/spurious) example  contains intentional (duplication/whitespace) ",
        "(whereas/) this example contains (/only) intentional duplication"
    ]),
    [
        "this example contains intentional duplication",
        "this spurious example contains intentional duplication",
        "this example contains intentional whitespace",
        "this spurious example contains intentional whitespace",
        "whereas this example contains intentional duplication",
        "whereas this example contains only intentional duplication",
        "this example contains only intentional duplication"
    ],
    "duplicates with whitespace"
);

const realistic_swaps = [
    {replace: "’", with: "'"},
    {replace: "?", with: ""},
    {replace: "–", with: " to "}, // Word can insert en-dashes in number ranges
    {replace: "\"", with: ""}
];  
tap.same(
    utter.expand([
        "does this (/spurious) example contain (duplication/whitespace) ?",
        "we don’t care  about (1963/1971)–(1965/1978)",
        "how about this \"input\"?!"
    ], realistic_swaps),
    [
        "does this example contain duplication",
        "does this spurious example contain duplication",
        "does this example contain whitespace",
        "does this spurious example contain whitespace",
        "we don't care about 1963 to 1965",
        "we don't care about 1971 to 1965",
        "we don't care about 1963 to 1978",
        "we don't care about 1971 to 1978",
        "how about this input!"
    ],
    "multiple character replacements with macros and whitespace"
);
