#!/usr/bin/env node
const readline = require("readline");
const utter = require("../utterance-expander.js");

const rl = readline.createInterface({
    input: process.stdin
});

rl.on("line", (line) => {
    process.stdout.write(utter.expand([line]).join("\n"));
    process.stdout.write("\n");
});