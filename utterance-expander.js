const Combinatorics = require("js-combinatorics");

const default_chars = [
    {replace: "’", with: "'"},   // curly apostrophe
    {replace: "&", with: "and"}, // ampersand
    {replace: "–", with: " to "}    // en dash
];

/**
  * Escapes all regex special chars so they're treated as literals
  * @param {String} unescaped String containing zero or more regex special characters.
  * @returns {String} String with all regex special characters escaped as literals.
  * http://developer.mozilla.org/docs/Web/JavaScript/Guide/Regular_Expressions#Using_special_characters
  */
function escapeAll(unescaped) {
    // $& means the whole matched string
    return unescaped.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

/**
  * Expands macros in input text, generating more lines of output.
  * @param {Array<string>} origin_text Original input lines with macros.
  * @param {Array<{replace:string,with:string}>} replacements String replacement objects.
  * @returns {Array<string>} Output lines of expanded text.
  */
exports.expand = (origin_text, replacements=default_chars) => {
    replacements.forEach(char => {
        origin_text = origin_text.map(
            line => line.replace(
                // Ensure all occurrences are replaced, see issue #3
                RegExp(escapeAll(char.replace), 'g'),
                char.with
            )
        );
    });

    const expanded_text = [];
    origin_text.forEach(line => {
        const word_stack = [];
        // HACK: this regex uses a negative lookahead. It won't handle nested
        //       parens; if we need that it'll be parser time.
        //       Regex fiddle: https://regex101.com/r/SavnRa/2
        line.split(/\s+(?![^(]*\))/).forEach(word => {
            if (word[0] === "(") {
                // first char is opening paren; this "word" is a macro; slice
                // off parens and split further on stroke/slash
                word_stack.push(word.slice(1, -1).split("/"));
            } else {
                // normal word; push onto the stack as single-element array
                // for the benefit of the spread operation below
                word_stack.push([word]);
            }
        });
        Combinatorics.cartesianProduct(...word_stack).toArray().forEach(
            utterance => {
                expanded_text.push(
                    // remove extraneous whitespace
                    utterance.join(" ").replace(/\s+/g, " ").trim()
                );
            }
        );
    });
    // use casting to/from an ES6 Set to filter out duplicates
    return [...new Set(expanded_text)];
};
